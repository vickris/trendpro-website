Rails.application.routes.draw do

  devise_for :users
  root 'welcome#index'
  resources :contacts, only: [:new, :create]
  resources :projects
  resources :tags
  resources :posts
  get "/about" => "welcome#about"
  get "/missing" => "welcome#missing"
  
  get '*path' => redirect('/missing')
  
end
