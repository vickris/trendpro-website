class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.string :client
      t.text :work_done
      t.string :link
      t.text :challenge
      t.text :project_goals
      t.text :solution
      t.text :results
      t.text :client_testimonial

      t.timestamps null: false
    end
  end
end
