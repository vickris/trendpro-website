class Project < ActiveRecord::Base
    has_many :project_tags
    has_many :tags, through: :project_tags
    has_attached_file :image, styles: { medium: "300x300#", thumb: "100x100#" }
    validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
    validates_attachment_presence :image
    belongs_to :user
    
    def all_tags
      project_tags.map(&:tag)
    end

    def tag_ids_to_array
      all_tags.inject([]){|tag_array, tag| tag_array << tag.id}
    end
end
