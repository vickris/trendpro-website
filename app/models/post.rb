class Post < ActiveRecord::Base
  belongs_to :category
  belongs_to :user
  has_attached_file :image, styles: { medium: "750x500>", thumb: "100x100#" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  validates_attachment_presence :image
end
