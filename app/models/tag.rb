class Tag < ActiveRecord::Base
    has_many :project_tags
    has_many :projects, through: :project_tags
    
    def self.all_sass
      where(:name => "saas")
    end
    
    def self.all_mobile
      where(:name => "mobile_app")
    end
    
    def self.all_websites
      where(:name => "website") 
    end
    
    def self.all_startups
      where(:name => "startup")
    end
    
    def self.all_agencies
      where(:name => "agency")
    end
end