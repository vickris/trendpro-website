class ProjectsController < ApplicationController
    before_action :find_project, only: [:show, :edit, :update, :destroy]
    before_action :authenticate_user!, except: [:index, :show]
  
    def index
      @projects = Project.all.order('created_at DESC')#.paginate(:page => params[:page], :per_page => 5)
    end
    
    def new
     @project = current_user.projects.build
    end
    
    def create
      @project = current_user.projects.build(project_params)
      if @project.save 
        flash[:success] = "Project was successfully created Chris"
        redirect_to @project
      else
        render :new
        flash[:danger] = "Seems something went wrong please try again"
      end
    end

    def show
    end
    
    def edit
    end
    
    def update
      if @project.update(project_params)
        redirect_to @project
        flash[:success] = "project was successfully updated"
      else
        render :edit
        flash[:danger] = "Ooops something went wrong..please try again"
      end
    end
  
    def destroy
      @project.destroy
      redirect_to projects_path
    end
    
    private
    
    def project_params
      params.require(:project).permit(:title, :category, :client, :work_done, :link, :challenge, :project_goals, :solution, :results, :client_testimonial, :image)
    end
    
    def find_project
      @project = Project.find(params[:id])
    end
    
    
end
