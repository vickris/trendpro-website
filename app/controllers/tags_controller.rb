class TagsController < ApplicationController
    def new
    @tag = Tag.new
    end
    
    def create
    @tag = Tag.new(tag_params)
        if @tag.save
          flash[:success] = "Tag was successfully created"
          redirect_to root_path
        else
          render :new
        end
    end
  
    def show
    @tag = Tag.find(params[:id])
    @projects = @tag.projects.order('created_at DESC')
    end
    

    def edit
    end
    
    def get_tags
      @sass_tags = Tag.all_sass
      @mobile_tags = Tag.all_mobile
      @website_tags = Tag.all_websites
      @agency_tags = Tag.all_agencies
      @startup_tags = Tag.all_startups
    end
    
    private
    def tag_params
      params.require(:tag).permit(:name)
    end
    
    
    
end
