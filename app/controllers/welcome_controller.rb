class WelcomeController < ApplicationController
  def index
    @posts = Post.order("created_at DESC").limit(4)
  end
  
  def about
  end
  
  def missing
  end
end
