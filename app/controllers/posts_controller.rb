class PostsController < ApplicationController
  before_action :find_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]
  
  def index
        if params[:category].blank?
            @posts = Post.all.order("created_at DESC")
        else
            @category_id = Category.find_by(name: params[:category]).id
            @posts = Post.where(category_id: @category_id).order("created_at DESC")
        end
  end
  
  def new
   @post = current_user.posts.build
  end
  
  def create
    @post = current_user.posts.build(post_params)
    if @post.save 
      flash[:success] = "Post was successfully created Chris"
      redirect_to @post
    else
      render :new
      flash[:danger] = "Seems something went wrong please try again"
    end
  end

  def show
    @posts = Post.order('created_at DESC').limit(4)
  end
  
  def edit
  end
  
  def update
    if @post.update(post_params)
      redirect_to @post
      flash[:success] = "Post was successfully updated"
    else
      render :edit
      flash[:danger] = "Ooops something went wrong..please try again"
    end
  end
  
  def destroy
    @post.destroy
    redirect_to posts_path
  end
  
  private
  
  def post_params
    params.require(:post).permit(:title, :body, :image)
  end
  
  def find_post
    @post = Post.find(params[:id])
  end
end